extends KinematicBody


export(int, 1, 2) var _player_id := 1
export var _spawn_point_node_path: NodePath
export(int, 1, 100) var _speed := 10
var _motion: Vector3
var _frozen := false
onready var _spawn_point := get_node(_spawn_point_node_path) as Position3D
onready var _animator := $AnimationPlayer as AnimationPlayer
onready var _particles := $Particles as Particles


func _physics_process(_delta: float) -> void:
	_move()
	_animate()
	_face_forward()


func reset() -> void:
	translation = _spawn_point.translation
	rotation = _spawn_point.rotation
	set_frozen(false)


func set_frozen(val: bool, player_id := 0) -> void:
	_frozen = val
	if _player_id == player_id:
		_particles.emitting = true


func _move():
	if not _frozen:
		var x := Input.get_action_strength("right_{0}".format([_player_id])) \
				- Input.get_action_strength("left_{0}".format([_player_id]))
		var z := Input.get_action_strength("down_{0}".format([_player_id])) \
				- Input.get_action_strength("up_{0}".format([_player_id]))
		_motion = Vector3(x, 0, z)
	# warning-ignore:return_value_discarded
		move_and_slide(_motion.normalized() * _speed, Vector3.UP)


func _animate() -> void:
	if _motion.length_squared() > 0.0 and not _frozen:
		_animator.play("Bouncy Walk", -1.0, 2.0)
	else:
		_animator.stop()


func _face_forward() -> void:
	if not _frozen and (_motion.x != 0 or _motion.z != 0):
		look_at(-_motion * _speed * 5, Vector3.UP)
