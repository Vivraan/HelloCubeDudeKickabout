extends RigidBody


const Player: Script = preload("res://players/player.gd")
export var _spawn_node: NodePath
onready var _spawn_position := get_node(_spawn_node) as Position3D
onready var tap := $Tap as AudioStreamPlayer

func reset() -> void:
	translation = _spawn_position.translation
	rotation = _spawn_position.rotation
	set_frozen(false)


func set_frozen(val: bool, __ := 0) -> void:
	axis_lock_angular_x = val
	axis_lock_angular_y = val
	axis_lock_angular_z = val
	axis_lock_linear_x = val
	axis_lock_linear_y = val
	axis_lock_linear_z = val
	sleeping = val


func _on_Ball_body_entered(body: Player) -> void:
	if body is Player and not tap.is_playing():
		tap.play()
