extends CanvasLayer


const SCORE_FMT := "{ {0} - {1} }"
const WIN_FMT := "Player {0} wins!"
onready var score := $Banner/HBoxContainer/Score as Label


func _on_Button_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()


func display_score(scores: PoolIntArray) -> void:
	score.text = SCORE_FMT.format([scores[0], scores[1]])


func game_over(winning_id: int) -> void:
	($Popup/VBoxContainer/Label as Label).text = WIN_FMT.format([winning_id])
	($Popup as Popup).popup_centered()

