extends SpotLight


export var player1: NodePath
export var player2: NodePath
onready var _player1: KinematicBody
onready var _player2: KinematicBody


func _ready() -> void:
	hide()


func set_frozen(__: bool = false, player_id: int = 0) -> void:
	var target: Vector3

	if player_id == 1:
		if not _player1:
			_player1 = get_node(player1) as KinematicBody
		target = _player1.translation
	else:
		if not _player2:
			_player2 = get_node(player2) as KinematicBody
		target = _player2.translation

	look_at(target, Vector3.UP)
	show()


func reset() -> void:
	hide()
