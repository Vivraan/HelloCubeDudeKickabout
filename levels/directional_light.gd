extends DirectionalLight

onready var _animator := $AnimationPlayer as AnimationPlayer
onready var _original_energy = light_energy


func _ready() -> void:
	reset()


func set_frozen(__: bool, _x: int) -> void:
	_animator.play("Fade")


func reset() -> void:
	light_energy = _original_energy
