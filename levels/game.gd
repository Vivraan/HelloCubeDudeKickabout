extends Spatial


const GUI: Script = preload("res://gui/gui.gd")
export(int, 1, 7) var target_score := 3
var _scores := PoolIntArray([0, 0])
onready var gui := $GUI as GUI


func _ready() -> void:
	gui.display_score(_scores)


func _on_GoalDetector_body_entered(_body: Node, goal_id: int) -> void:
	var opponent_id = 3 - goal_id
	get_tree().call_group("actor", "set_frozen", true, opponent_id)
	($Timer as Timer).start()
	($Airhorn as AudioStreamPlayer).play()
	_update_score(opponent_id)


func _on_Timer_timeout() -> void:
	get_tree().call_group("actor", "reset")


func _update_score(player_id: int) -> void:
	_scores[player_id - 1] += 1
	var leading_score = _scores[player_id - 1]
	gui.display_score(_scores)
	_check_for_end_game(player_id, leading_score)


func _check_for_end_game(winner_id: int, score: int) -> void:
	if score == target_score:
		($Timer as Timer).queue_free()
		gui.game_over(winner_id)
